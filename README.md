# Gnome Mouse Hack

This is small script which enables mouse navigation trhough Gnome (v.42+) workspaces using mouse buttons:

Scroll wheel click - Overwiew
Mouse button 5 (Forward) - Next workspace
Mouse button 6 (Backward) - Previous workspace

## Prerequisites:

This script depends on `evemu-record` tool.

Effectively unbind extra mouse buttons by modifying udev/hwdb rules:

### 1. Identify scancodes:
```bash
sudo evemu-record /dev/input/by-path/*event-mouse \
| stdbuf -oL grep --only-matching "MSC_SCAN.*" \
| awk '{printf $2" 0x"; printf "%x\n", $2}'
```
or `sudo libinput debug-events`


### 2. Map buttons as keyboard keys in order to disable their functionality 

(see /usr/include/linux/input-event-codes.h for valid keycodes). For example:
```
    # /etc/udev/hwdb.d/mouse-unbind.hwdb
    evdev:input:*
     KEYBOARD_KEY_90004=key_n
     KEYBOARD_KEY_90005=key_i
     KEYBOARD_KEY_90006=key_l
```
### Alternative method for both keyboard and mouse:

You can also check the keyboard or mouse codes by running `sudo evemu-record`, select the correct keyboard or mouse from the list and then press the button you are interested in.
You should see the button code in the output.

## Adjusting the script
Unfortunately script won't work out of the box because of the hardware differences and their detection sequence in the system, hence you need to open the `mouse-hack.sh` script and update following with the data you gathered from previous steps:

```
MOUSE_DEVICE=/dev/input/eventX
KEYBOARD_DEVICE=/dev/input/eventX
EVENT_CODE="MSC_SCAN"
MOUSE_BUTTON_SIDE_DOWN="XXXXXX"
MOUSE_BUTTON_SIDE_UP="YYYYYY"
MOUSE_BUTTON_SWITCH="ZZZZZZ"
```

If you provide correct parameters, the script should start working right away.

## Making it autoload
Gnome doesn't allow running shell scripts at startup, so `gnome-mouse-hack.desktop` file is needed to be copied to `~/.config/autostart/` folder.

In addition to that please adjust path to where your script is actually located in the `desktop` file.