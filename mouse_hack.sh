#!/usr/bin/env bash
# -*- indent-tabs-mode: nil; tab-width: 4 -*-
#
# An ugly way to bind extra mouse buttons under GNOME/Wayland or otherwise.
#
# Prerequisites:
#
# Effectively unbind extra mouse buttons by modifying udev/hwdb rules:
# 1. Identify scancodes:
#    sudo evemu-record /dev/input/by-path/*event-mouse \
#    | stdbuf -oL grep --only-matching "MSC_SCAN.*" \
#    | awk '{printf $2" 0x"; printf "%x\n", $2}'
#
#    or `sudo libinput debug-events`
#
# 2. Map buttons as keyboard keys in order to disable their functionality (see 
#    /usr/include/linux/input-event-codes.h for valid keycodes). For example:
#    # /etc/udev/hwdb.d/mouse-unbind.hwdb
#    evdev:input:*
#     KEYBOARD_KEY_90004=key_n
#     KEYBOARD_KEY_90005=key_i
#     KEYBOARD_KEY_90006=key_l
# 3. Apply configuration:
#    sudo systemd-hwdb update && sudo udevadm trigger
# 4. Verify:
#    systemd-hwdb query "evdev:input:*"
#
# Depends: evemu-tools

MOUSE_DEVICE=/dev/input/event2
KEYBOARD_DEVICE=/dev/input/event8
EVENT_CODE="MSC_SCAN"
MOUSE_BUTTON_SIDE_DOWN="589828"  # 0x90004
MOUSE_BUTTON_SIDE_UP="589829"  # 0x90005
MOUSE_BUTTON_SWITCH="589827"  # 0x90006

function key() {
    sudo evemu-event "$KEYBOARD_DEVICE" --sync --type EV_KEY \
        --code $1 --value $2;
}

function button() {
    sudo evemu-event "$MOUSE_DEVICE" --sync --type EV_KEY \
        --code $1 --value $2;
}

while read line; do
    if [ -z "${line##*$EVENT_CODE*}" ]; then
        read nextline
        button=${line##* }
        value=${nextline##* }

        case "$button$value" in 
            # "$MOUSE_BUTTON_SWITCH""1")  # Toggle overview
            #     key KEY_LEFTMETA 1
            #     key KEY_LEFTMETA 0
            # ;;
            "$MOUSE_BUTTON_SIDE_UP""1")  # Move to workspace above
                key KEY_LEFTMETA 1
                key KEY_PAGEDOWN 1
                key KEY_PAGEDOWN 0
                key KEY_LEFTMETA 0
            ;;
            "$MOUSE_BUTTON_SIDE_DOWN""1")  # Move to workspace below
                key KEY_LEFTMETA 1
                key KEY_PAGEUP 1
                key KEY_PAGEUP 0
                key KEY_LEFTMETA 0
            ;;
	    "$MOUSE_BUTTON_SWITCH""1")  # Middle button press
		key KEY_LEFTMETA 1
		key KEY_LEFTMETA 0
	    ;;
        esac
    fi
done < <(sudo evemu-record "$MOUSE_DEVICE")
